# -*- mode: ruby -*-
# vi: set ft=ruby :
#
# Copyright (c) 2017-2018 Catalyst.net Ltd
#
# This file is part of puppet-masterless.
#
# puppet-masterless is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# puppet-masterless is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with puppet-masterless. If not, see http://www.gnu.org/licenses/
#

Gem::Specification.new do |spec|
  spec.name          = 'puppet-masterless'
  spec.version       = '0.2.4'
  spec.authors       = 'Evan Hanson'
  spec.email         = 'evanh@catalyst.net.nz'
  spec.homepage      = 'https://gitlab.com/catalyst-it/puppet-masterless'
  spec.summary       = %q{Apply a local Puppet project to a remote host}
  spec.description   = %q{Packages and applies a Puppet project to a remote host.}
  spec.bindir        = 'bin'
  spec.license       = 'GPLv3'
  spec.files         = ['README.md', 'LICENSE.md', 'man/puppet-masterless.1']
  spec.executables   = ['puppet-masterless']

  spec.add_development_dependency 'rake', '~> 12.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
